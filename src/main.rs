use serde_json::Value;
use std::fs;
mod item;
use item::{Bracket, Parent};

fn read_json(path: &str) -> Value {
    {
        let file_content = fs::read_to_string(path).expect("JaSON: error reading file");
        serde_json::from_str::<Value>(&file_content).expect("JaSON: error serializing to JSON")
    }
}

fn main() {
    let sales_and_products = read_json("./data/sales.json");
    let data = serde_json::to_string_pretty(&sales_and_products).expect("Error");
    println!("{}", data);
    let s = sales_and_products
        .as_object()
        .expect("Cannot transfrom to the object");
    for (key, name) in s.iter() {
        let pr: Parent = Parent::new(key.to_string(), Bracket::new(s[key].to_string()), 2);
        println!("{}", pr.print());
        println!("{}", s[key]);
        println!("{:?}\n\n", name);
    }
}
