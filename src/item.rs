use std::{any::Any};

#[derive(Debug)]
pub enum Bracket {
    Square,
    Cursive,
}

impl Bracket {
    pub fn new(bracket: String) -> Self {
        match bracket.chars().next().unwrap().to_string().as_str() {
            "[" => Bracket::Square,
            "{" => Bracket::Cursive,
            _ => Bracket::Cursive,
        }
    }
}

pub struct Parent {
    name: String,
    char: Bracket,
    pr: u32,
}

impl Parent {
    pub fn new(name: String, char: Bracket, pr: u32) -> Self {
        Parent {
            name,
            char,
            pr,
        }
    }

    pub fn print(&self) -> String {
        let bracket = match self.char {
            Bracket::Cursive => "{{...}}",
            Bracket::Square => "[...]",
        };
        let mut result = format!("{}: {}", self.name, bracket);
        for _ in 0..self.pr as i32 {
            result = format!("    {}", result);
        }
        result
    }
}

pub struct Item {
    parent: Parent, 
    value: Box<dyn Any>,
    name: String,
}

impl Item {
    pub fn new<T: 'static>(parent: Parent, value: T, name: String) -> Self {
        Item { parent, value: Box::new(value), name }
    }
    pub fn print(&self) -> String {
        let mut result = String::new();
        result = format!("\"{}\": {}", self.name, self.value.downcast_ref::<str>().to_string());
        for _ in 0..self.parent.pr as i32 {
            result = format!("    {}", result)
        }
        result
    }
}
